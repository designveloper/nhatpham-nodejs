var fs = require('fs');
var data = '';
var readStream = fs.createReadStream(__dirname + '/data.txt', 'utf-8');

readStream.once('data', function () {
  console.log('\n\n\n');
  console.log('Start reading file');
  console.log('\n\n\n');
});

readStream.on('data', function (chunk) {
  process.stdout.write(` chunk: ${chunk.length} |`);
  data += chunk;
});
var writeStream = fs.createWriteStream(__dirname + '/file.txt', 'utf-8');
readStream.pipe(writeStream);

readStream.on('end', function () {
  console.log('\n\n\n');
  console.log('Stop reading file' + data.length);
  console.log('\n\n\n');
});