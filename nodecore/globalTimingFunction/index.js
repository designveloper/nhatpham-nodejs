var waitTime = 4000;
var waitInterval = 500;
var currentTime = 0;
var percentWaited = 0;

function writeWaitingPercent(p) {
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write(`Waiting ... ${p} %`);
}
var interval = setInterval(function () {
  currentTime += waitInterval;
  percentWaited = Math.floor((currentTime / waitTime) * 100);
  writeWaitingPercent(percentWaited);
 // console.log(`Waiting ${currentTime / 1000} seconds...`);
}, waitInterval);
setTimeout(function () {
  clearInterval(interval);
  writeWaitingPercent(100);
  console.log('\nDone');
}, waitTime);
console.log('Waiting for it');