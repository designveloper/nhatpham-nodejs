module.exports = function (grunt) {
  grunt.initConfig({
    jshint: {
      files: ["*.js"],
      options: {
        esnext: true,
        globals: {
          jQuery: true
        }
      }
    },
    less: {
      production: {
        files: {
          "style.css": "style.less"
        }
      }
    },
    autoprefixer: {
      single_file: {
        src: "style.css",
        dest: "style.css"
      }
    },
    browserify: {
      client: {
        src: 'app-client.js',
        dest: 'bundle.js'
      }
    },
    watch: {
      css: {
        files: ["*.less"],
        tasks: ["css"]
      },
      scripts: {
        files:["app-client.js"],
        tasks: ["js"]
      }
    }
  });
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.loadNpmTasks("grunt-autoprefixer");
  grunt.loadNpmTasks("grunt-browserify");
  grunt.loadNpmTasks("grunt-contrib-watch");

  grunt.registerTask("js", ["browserify"]);
  grunt.registerTask("css", ["less","autoprefixer"]);
  grunt.registerTask("default", ["js","css", "js"]);
};