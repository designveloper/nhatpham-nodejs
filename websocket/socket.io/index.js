var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app).listen(4000);
var io = require('socket.io')(server);
var fs = require('fs');
app.use(express.static(__dirname + '/public'));
io.on("connection", function (socket) {
  socket.on("chat", function (message) {
    socket.broadcast.emit("message", message);
  });
  socket.emit("message", "Welcome to Cyber Chat");

});
app.get('/', function (req, res) {
  var html = fs.readFileSync(__dirname + '/index.html', 'utf-8');
  res.send(html);
});
console.log('Starting socket app');