var https = require('https');
module.exports = {
  printName: function (name) {
    return name.last + ', ' + name.first;
  },
  loadWiki: function (person, callback) {
    var url = `https://en.wikipedia.org/wiki/${person.first}_${person.last}`;
    https.get(url, function (res) {
      var body = '';
      res.setEncoding('UTF-8');
      res.on('data', function (chunk) {
        body += chunk;
        //console.log('dat');
      });
      res.on('end', function () {
        callback(body);
      });
    });
  },
}