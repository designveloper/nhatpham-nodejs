var chai = require('chai');
var expect = chai.expect;
var tools = require('./tools.js');
var nock = require('nock');
describe('Tools', function () {
  describe('printName()', function () {
    it('should print the last name first', function () {
      var result  = tools.printName({first: "Alex", last: "Banks"})
      expect(result).to.equal("Banks, Alex");
    });
  });
  describe('LoadWiki()', function () {
    before(function(){
      nock('https://en.wikipedia.org')
      .get('/wiki/Abraham_Lincoln')
      .reply(200,'Mock Abraham Lincoln Page');
    });
    it('load ..' , function(done){
      tools.loadWiki({first: 'Abraham', last: 'Lincoln'} , function(html){
        //console.log(html);
        expect(html).to.equal('Mock Abraham Lincoln Page');
        done();
      });      
    });
  });
});