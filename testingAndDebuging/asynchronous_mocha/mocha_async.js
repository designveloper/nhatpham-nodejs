var chai = require('chai');
var expect = chai.expect;
var tools = require('./tools.js');
describe('Tools', function () {
  describe('printName()', function () {
    it('should print the last name first', function () {
      var result  = tools.printName({first: "Alex", last: "Banks"})
      expect(result).to.equal("Banks, Alex");
    });
  });
  describe('LoadWiki()', function () {
    it('load ..' , function(done){
      tools.loadWiki({first: 'Abraham', last: 'Lincoln'} , function(html){
        //console.log(html);
        expect(html).to.be.ok;
        done();
      });      
    });
  });
});