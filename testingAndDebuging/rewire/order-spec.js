var expect = require('chai').expect;
var rewire = require('rewire');
var order = rewire('./order');

describe('Ordering Item', function () {
  beforeEach(function () {
    this.testData = [
      { sku: 'AAA', qty: 10 },
      { sku: 'BBB', qty: 0 },
      { sku: 'CCC', qty: 4 },
    ]
    order.__set__('inventoryData', this.testData);
  });
  it('order an item when there are enough in the stock', function(done){
    order.orderItem("CCC",3 , function(){
      done();
    });
  });
});
