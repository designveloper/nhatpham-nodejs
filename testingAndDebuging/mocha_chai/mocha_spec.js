var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();

describe('indexOf', function () {
  it('should return -1 when the value is not present', function () {
    var array = [1, 2, 3, 4];
    assert.equal(-1, array.indexOf(5));
  });
});