var expect = require('chai').expect;
var request = require('supertest');
var rewire = require('rewire');
var app = rewire('./app');

describe('Dictionary App', function () {
  it('load the home page', function (done) {
    request(app).get('/').expect(200).end(done);
  });
  describe('Dictionary API', function () {

    beforeEach(function () {
      this.defs = [
        { term: 'One', defined: 'Term one define' },
        { term: 'Two', defined: 'Term two define' },
      ]

      app.__set__('skierTerms', this.defs);
    });
    it('GET Dictionary API', function (done) {
      request(app).get('/dictionary-api').expect(200).end(done);
    });
    it('POSTS Dictionary API', function (done) {
      request(app).post('/dictionary-api').send({ "term": "three", "defined": "Term Three Defined" }).expect(200).end(done);
    });
    it('DELETES Dictionary API', function(done){
      request(app).delete('/dictionary-api/One')
      .expect(200)
      .end(done)
    });
  });
});
