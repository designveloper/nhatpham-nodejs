var Person = require('./Person.js');
var util = require('util');

var nhat = new Person('Minh Nhat');
nhat.on('speak', function (said) {
  console.log(`${this.name}: ${said}`);
});

nhat.emit('speak', 'Nothing is impossible!');