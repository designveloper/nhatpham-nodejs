var EventEmitter = require('events').EventEmitter;
var util = require('util');

var Person = function (name) {
  this.name = name;
}
util.inherits(Person, EventEmitter);
var nhat = new Person('Minh Nhat');
nhat.on('speak', function (said) {
  console.log(`${this.name}: ${said}`);
});

nhat.emit('speak', 'Nothing is impossible!');