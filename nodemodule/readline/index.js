var readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var realPerson = {
  name :'',
  sayings :[]
}
rl.question('What is the name of a real person', (answer) =>{
    rl.setPrompt(`What would ${realPerson.name} say?`);
    rl.prompt();
    rl.on('line', function(saying){
      realPerson.sayings.push(saying.trim());
      if(saying+''.toLocaleLowerCase().trim() === 'exit'){
        rl.close();
      }
      else {
        rl.setPrompt(`What else would ${realPerson.name} say? ('exit' to leave)`);
        rl.prompt();
      }
    });
});
  rl.on('close',function(){
    console.log('%s is a real person that say %j', realPerson.name, realPerson.sayings);
    process.exit();
  });