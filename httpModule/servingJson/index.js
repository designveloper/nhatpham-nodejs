var http = require('http');
var fs = require('fs');
var path = require('path');
var data = require('./data/user.json');
var server = http.createServer(function (req, res) {
  if (req.url === '/') {
    console.log('root');
    fs.readFile(__dirname + '/index.html', 'utf-8', function (err, html) {
      res.writeHead(200, { 'Content-Type': 'text/json' });
      res.end(JSON.stringify(data));
    });
  } else {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('404 File Not found');
  }
  // res.end("Hello World");
});
server.listen(4000);