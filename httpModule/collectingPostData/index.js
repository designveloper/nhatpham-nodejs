var http = require('http');
var fs = require('fs');
var path = require('path');

var server = http.createServer(function (req, res) {
  if (req.url === '/') {
    console.log('root');
    fs.readFile(__dirname + '/index.html', 'utf-8', function (err, html) {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(html);
    });
  } else if (req.method === 'POST') {
    var body = '';
    req.on('data', function (chunk) {
      body += chunk;
    });
    req.on('end', function(){
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(body);
    });
  } else {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('404 File Not found');
  }
  // res.end("Hello World");
});
server.listen(4000);